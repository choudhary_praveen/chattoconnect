package com.example.chattoconnect.pushNotification;

public class Data {
    private String users;
    private int icon;
    private String body,title,sented;

    public Data() {

    }

    public Data(String users, int icon, String body, String title, String sented) {
        this.users = users;
        this.icon = icon;
        this.body = body;
        this.title = title;
        this.sented = sented;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSented() {
        return sented;
    }

    public void setSented(String sented) {
        this.sented = sented;
    }
}
