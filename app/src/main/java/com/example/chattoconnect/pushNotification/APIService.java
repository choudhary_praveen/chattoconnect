package com.example.chattoconnect.pushNotification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers({
                    "Content-Type:application/json",
                    "Authorization:key=AAAAepY4Fxw:APA91bEjOraA5HlCo8Ra8ubCKch2eO2M3hNi0fd0Z90rIl6pAJ42uGf7ZTLiaOcmmY84Ix0SSW_nrFRh18TrpZDwga1yRIxmW0yTkDcRqmLXm55qBE1Wx6r7asO0U2_XdfdMheGjXT2J"
            })
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
