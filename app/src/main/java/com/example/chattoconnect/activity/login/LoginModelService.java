package com.example.chattoconnect.activity.login;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginModelService implements LoginContractInterface.Model{

   private LoginPersenter loginPersenter;
    FirebaseAuth firebaseAuth;

   public LoginModelService(LoginContractInterface.Presenter loginPersenter)
   {
       this.loginPersenter = (LoginPersenter) loginPersenter;
   }

    public LoginModelService() {

    }


    @Override
    public void doSignIn(String email1,String password1) {


        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        // progressDialog.show();
        if (TextUtils.isEmpty(email1) || TextUtils.isEmpty(password1)) {
            loginPersenter.loginfailed("Enter Data");
            loginPersenter.progressDialog(false);

        } else if (!email1.matches(emailPattern)) {
            loginPersenter.loginfailed("Invalid Email");
            loginPersenter.progressDialog(false);

        } else if (password1.length() < 6) {
            loginPersenter.loginfailed("Enter valid password");
            loginPersenter.progressDialog(false);

        } else {
            firebaseAuth = FirebaseAuth.getInstance();
            firebaseAuth.signInWithEmailAndPassword(email1, password1).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        loginPersenter.progressDialog(true);
                        loginPersenter.signIn();


                    } else {
                        loginPersenter.loginfailed("Error in Login");
                        loginPersenter.progressDialog(false);

                        //progressDialog.dismiss();
                        // Toast.makeText(LoginActivity.this, "Error in Login", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }
}
