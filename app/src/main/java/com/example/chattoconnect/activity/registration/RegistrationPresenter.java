package com.example.chattoconnect.activity.registration;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;

import com.example.chattoconnect.activity.login.LoginContractInterface;
import com.example.chattoconnect.activity.login.LoginModelService;
import com.example.chattoconnect.modal.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class RegistrationPresenter implements RegistrationContractInterface.Presenter{

    private RegistrationContractInterface.Model model;
    private RegistrationContractInterface.View view;

    public RegistrationPresenter(RegistrationContractInterface.View view) {
        this.view = view;
        this.model = new RegistrationModelService(this);
    }


    @Override
    public void doSignUp(String name, String email, String password, String cnf_password, Uri imageUri) {
     model.doSignIn(name, email, password, cnf_password, imageUri);
    }

    @Override
    public void loginfailed(String error) {
     view.showLoginFailed(error);
    }

    @Override
    public void progressDialog(boolean dialog) {
        if(dialog==true)
        {
            view.showProgress();
        }
        else {
            view.hideProgress();
        }

    }

    @Override
    public void signUp() {
         view.signUnBtn1();
    }

    @Override
    public void getImageUri(String URI) {
        model.setImageUri(URI);

    }
}
