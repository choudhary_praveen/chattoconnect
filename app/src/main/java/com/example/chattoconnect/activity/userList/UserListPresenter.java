package com.example.chattoconnect.activity.userList;

import androidx.annotation.NonNull;

import com.example.chattoconnect.modal.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class UserListPresenter implements UserListContractInterface.Presenter{
    private UserListContractInterface.Model model;
    private UserListContractInterface.View view;

    public UserListPresenter(UserListContractInterface.View view) {

        this.view = view;
        this.model = new UserListModelService(this);
    }

    public UserListPresenter() {
    }

    @Override
    public void loggedOut() {
     view.logoutActivity();
    }



    @Override
    public void logOutModel() {
    model.doLogout();
    }

    @Override
    public void onModelUpdate(ArrayList<Users> list) {
       view.updateList(list);
    }


}
