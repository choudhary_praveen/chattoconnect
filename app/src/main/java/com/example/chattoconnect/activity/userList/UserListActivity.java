package com.example.chattoconnect.activity.userList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chattoconnect.activity.login.LoginActivity;
import com.example.chattoconnect.R;
import com.example.chattoconnect.activity.registration.RegistrationActivity;
import com.example.chattoconnect.databinding.ActivityUserListBinding;
import com.example.chattoconnect.modal.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity implements UserListContractInterface.View {
    FirebaseAuth firebaseAuth;
    RecyclerView mainUserRecyclerView;
    UserListAdapter adapter;
    ArrayList<Users> usersArrayList;
    private boolean doubleBackToExitPressedOnce = false;
    ActivityUserListBinding activityUserListBinding;
    private UserListPresenter userListPresenter;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityUserListBinding = DataBindingUtil.setContentView(this,R.layout.activity_user_list);
        activityUserListBinding.setIuser(this);
        Log.d("debug1","inhomeActivity");
        userListPresenter = new UserListPresenter(this);
        firebaseAuth = FirebaseAuth.getInstance();
        usersArrayList = new ArrayList<>();
        adapter = new UserListAdapter(UserListActivity.this,usersArrayList);
        mainUserRecyclerView = activityUserListBinding.userRecyclerView;
        mainUserRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainUserRecyclerView.setAdapter(adapter);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        if(firebaseAuth.getCurrentUser()==null)
        {
            Intent intent = new Intent( UserListActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        progressDialog.show();

    }
    public void onLogout()
    {
        Dialog dialog = new Dialog(UserListActivity.this,R.style.Dialoge);
        dialog.setContentView(R.layout.dialog_layout);
        TextView logout_no,logout_yes;

        logout_no=dialog.findViewById(R.id.logout_no);
        logout_yes=dialog.findViewById(R.id.logout_yes);
        logout_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userListPresenter.loggedOut();
               // FirebaseAuth.getInstance().signOut();
                //startActivity(new Intent(UserListActivity.this,LoginActivity.class));
            }
        });
        logout_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }



    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        doubleBackToExitPressedOnce = true;

    }

    @Override
    public void logoutActivity() {
        startActivity(new Intent(UserListActivity.this,LoginActivity.class));

    }
    @Override
    public void updateList(ArrayList<Users> list)
    {
        progressDialog.dismiss();
        usersArrayList.clear();
        usersArrayList.addAll(list);
        adapter.notifyDataSetChanged();
    }
}


