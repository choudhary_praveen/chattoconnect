package com.example.chattoconnect.activity.chatActivity;

import com.example.chattoconnect.modal.Messages;
import com.example.chattoconnect.modal.Users;

import java.util.ArrayList;
import java.util.List;

public interface ChatContractInterface {
    interface View {
        //public void addMessageToView();
         void dataToRecyclerView(ArrayList<Messages> messages);
    }
    interface Presenter {
      void sendMessageToView(ArrayList<Messages> messages);
      void sendDataToModel( String receverUid, String RImage);
      void sendMessage(Messages messages);


    }
    interface Model
    {
         void sendMessageModel(Messages messages);
       //  void getDataOnRefresh();
         void dataFromPersenter( String receverUid, String RImage);
    }
}
