package com.example.chattoconnect.activity.registration;

import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.example.chattoconnect.modal.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class RegistrationModelService implements RegistrationContractInterface.Model{

    private RegistrationPresenter registrationPresenter;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    FirebaseStorage firebaseStorage;
    String imageURI;

    public RegistrationModelService(RegistrationContractInterface.Presenter registrationPresenter){
        this.registrationPresenter = (RegistrationPresenter) registrationPresenter;
    }
    public RegistrationModelService()
    {

    }

    @Override
    public void doSignIn(String name, String email, String password, String cnf_password, Uri imageUri) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        if(TextUtils.isEmpty(email)||TextUtils.isEmpty(password)||TextUtils.isEmpty(name)||TextUtils.isEmpty(cnf_password))
        {
            registrationPresenter.loginfailed("Enter Data");
            registrationPresenter.progressDialog(false);
        }
        else if (!email.matches(emailPattern))
        {
            registrationPresenter.loginfailed("Invalid Email Address");
            registrationPresenter.progressDialog(false);
        }
        else if(password.length()<6)
        {
            registrationPresenter.loginfailed("Password length should be greater then 6 digit");
            registrationPresenter.progressDialog(false);

        }
        else if(!password.equals(cnf_password))
        {
            registrationPresenter.loginfailed("Both Password Should Be same");
            registrationPresenter.progressDialog(false);

        }
        else {


            firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        DatabaseReference reference=firebaseDatabase.getReference().child("user").child(firebaseAuth.getUid());
                        StorageReference storageReference=firebaseStorage.getReference().child("uplod").child(firebaseAuth.getUid());

                        if(imageUri!=null)
                        {
                            storageReference.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                    if(task.isSuccessful())
                                    {
                                        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                imageURI=uri.toString();
                                                Users users=new Users(firebaseAuth.getUid(),name,email,imageURI);
                                                reference.setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful())
                                                        {
                                                            registrationPresenter.progressDialog(true);
                                                            registrationPresenter.signUp();

                                                        }else {
                                                            registrationPresenter.loginfailed( "Error in Creating a New user");
                                                            registrationPresenter.progressDialog(false);

                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }else {

                            imageURI="https://firebasestorage.googleapis.com/v0/b/quickchatapp-a181b.appspot.com/o/profile_image.png?alt=media&token=a6748395-5c27-492d-b73b-d6859d4b5d15";
                            Users users=new Users(firebaseAuth.getUid(),name,email,imageURI);
                            reference.setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful())
                                    {   registrationPresenter.progressDialog(true);
                                        registrationPresenter.signUp();
                                    }else {
                                        registrationPresenter.loginfailed("Error in Creating a New user");
                                        registrationPresenter.progressDialog(false);

                                    }
                                }
                            });
                        }

                    } else {
                        registrationPresenter.loginfailed("User Email Already Exist");
                        registrationPresenter.progressDialog(false);

                    }
                }
            });
        }
    }

    @Override
    public void setImageUri(String uri) {
       imageURI = uri;
    }
}
