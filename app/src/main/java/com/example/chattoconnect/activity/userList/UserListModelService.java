package com.example.chattoconnect.activity.userList;

import androidx.annotation.NonNull;

import com.example.chattoconnect.modal.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UserListModelService implements UserListContractInterface.Model {
    private UserListPresenter userListPresenter;
   // ArrayList<Users> usersArrayList;
    // FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;

    public UserListModelService(UserListContractInterface.Presenter userListPresenter) {
        this.userListPresenter = (UserListPresenter) userListPresenter;
        reference = FirebaseDatabase.getInstance().getReference().child("user");
        this.invoke();
    }

    public UserListModelService() {
    }

    @Override
    public void getDataFromServer() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //   progressDialog.show();
                ArrayList<Users> usersArrayList = new ArrayList<>();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Users users = dataSnapshot.getValue(Users.class);
                    if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(users.getUid())) {
                        continue;
                    } else {
                        usersArrayList.add(users);
                    }


                }
                userListPresenter.onModelUpdate(usersArrayList);
                //adapter.notifyDataSetChanged();
                //    progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void invoke() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //   progressDialog.show();
                    ArrayList<Users> usersArrayList = new ArrayList<>();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Users users = dataSnapshot.getValue(Users.class);
                    if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(users.getUid())) {
                        continue;
                    } else {
                        usersArrayList.add(users);
                    }


                }
                userListPresenter.onModelUpdate(usersArrayList);
                //adapter.notifyDataSetChanged();
                //    progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    //listenToUserList();


    @Override
    public void dataFromModel(ArrayList<Users> list) {
        // usersArrayList = list;
    }

    @Override
    public void doLogout() {
        FirebaseAuth.getInstance().signOut();
        userListPresenter.logOutModel();
    }

    private void listenToUserList() {

    }
}
