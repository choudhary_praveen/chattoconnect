package com.example.chattoconnect.activity.chatActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chattoconnect.R;
import com.example.chattoconnect.databinding.ReciverLayoutItemBinding;
import com.example.chattoconnect.databinding.SenderLayoutItemBinding;
import com.example.chattoconnect.databinding.UserRecyclerviewRowBinding;
import com.example.chattoconnect.modal.Messages;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

//import static com.example.chattoconnect.activity.chatActivity.ChatActivity.rImage;
//import static com.example.chattoconnect.activity.chatActivity.ChatActivity.sImage;

public class ChatMessageAdapter extends RecyclerView.Adapter {
    Context context;
    ArrayList<Messages> messagesArrayList;
    int ITEM_SEND = 1;
    int ITEM_RECIVE = 2;

    public ChatMessageAdapter(Context context, ArrayList<Messages> messagesArrayList) {
        this.context = context;
        this.messagesArrayList = messagesArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == ITEM_SEND) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            SenderLayoutItemBinding senderLayoutItemBinding = SenderLayoutItemBinding.inflate(layoutInflater, parent, false);
            return new SenderViewHolder(senderLayoutItemBinding);

         //  View view = LayoutInflater.from(context).inflate(R.layout.sender_layout_item, parent, false);
          //  return new SenderViewHolder(view);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            ReciverLayoutItemBinding reciverLayoutItemBinding = ReciverLayoutItemBinding.inflate(layoutInflater, parent, false);
            return new ReciverViewHolder(reciverLayoutItemBinding);


         //   View view = LayoutInflater.from(context).inflate(R.layout.reciver_layout_item, parent, false);
         //   return new ReciverViewHolder(view);
        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Messages messages = messagesArrayList.get(position);

        if (holder.getClass() == SenderViewHolder.class) {
            ((SenderViewHolder) holder).senderLayoutItemBinding.setMessages(messages);
            ((SenderViewHolder) holder).senderLayoutItemBinding.executePendingBindings();
      //      SenderViewHolder viewHolder = (SenderViewHolder) holder;
        //    viewHolder.txtmessage.setText(messages.getMessage());

        } else {
           ((ReciverViewHolder) holder).reciverLayoutItemBinding.setMessages(messages);
            ((ReciverViewHolder) holder).reciverLayoutItemBinding.executePendingBindings();
        //   ReciverViewHolder viewHolder = (ReciverViewHolder) holder;
        //    viewHolder.txtmessage.setText(messages.getMessage());

        }

    }

    @Override
    public int getItemCount() {
        return messagesArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Messages messages = messagesArrayList.get(position);
        if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(messages.getSenderId())) {
            return ITEM_SEND;
        } else {
            return ITEM_RECIVE;
        }
    }

    class SenderViewHolder extends RecyclerView.ViewHolder {
         SenderLayoutItemBinding senderLayoutItemBinding;

          public SenderViewHolder(@Nullable SenderLayoutItemBinding senderLayoutItemBinding) {
               super(senderLayoutItemBinding.getRoot());
               this.senderLayoutItemBinding = senderLayoutItemBinding;
           }



      /*  TextView txtmessage;

        public SenderViewHolder(@NonNull View itemView) {
            super(itemView);

           // circleImageView = itemView.findViewById(R.id.profile_image);
            txtmessage = itemView.findViewById(R.id.txtMessages);

        }*/
    }
    public void updateData() {

    }


   class ReciverViewHolder extends RecyclerView.ViewHolder {

        ReciverLayoutItemBinding reciverLayoutItemBinding;

        public ReciverViewHolder(@NonNull ReciverLayoutItemBinding reciverLayoutItemBinding) {
            super(reciverLayoutItemBinding.getRoot());
            this.reciverLayoutItemBinding = reciverLayoutItemBinding;

        }

       /*
        TextView txtmessage;

        public ReciverViewHolder(@NonNull View itemView) {
            super(itemView);


            txtmessage = itemView.findViewById(R.id.txtMessages);

        }*/
    }

}
