package com.example.chattoconnect.activity.login;

public interface LoginContractInterface {
    interface View {

        void showLoginFailed(String error);
        void signInBtn1();
        void showProgress();
        void hideProgress();
    }
    interface Presenter {
        void doLogin(String email,String password);
        void loginfailed(String error);
        void progressDialog(boolean dialog);
        void signIn();
    }
    interface Model{
        void doSignIn(String email,String password);
    }
}
