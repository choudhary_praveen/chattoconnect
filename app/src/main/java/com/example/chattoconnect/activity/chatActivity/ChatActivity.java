package com.example.chattoconnect.activity.chatActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.chattoconnect.R;
import com.example.chattoconnect.databinding.ActivityChatBinding;
import com.example.chattoconnect.modal.Messages;
import com.example.chattoconnect.modal.Users;
import com.example.chattoconnect.pushNotification.APIService;
import com.example.chattoconnect.pushNotification.Client;
import com.example.chattoconnect.pushNotification.Data;
import com.example.chattoconnect.pushNotification.MyResponse;
import com.example.chattoconnect.pushNotification.Sender;
import com.example.chattoconnect.pushNotification.Token;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity implements ChatContractInterface.View {

    String receiverImage,receiverName,receiverUid,senderUid,senderRoom,receiverRoom;
    String userid;
    RecyclerView messageAdapter;
    ArrayList<Messages> messagesArrayList;
    ChatMessageAdapter adapter;
    ActivityChatBinding activityChatBinding;
    private ChatPersenter chatPersenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityChatBinding = DataBindingUtil.setContentView(this,R.layout.activity_chat);
        activityChatBinding.setChating(this);
        chatPersenter = new ChatPersenter(this);


        receiverImage = getIntent().getStringExtra("ReciverImage");
        receiverName = getIntent().getStringExtra("name");
        receiverUid = getIntent().getStringExtra("uid");
        userid = receiverUid;
        Users users = new Users();
        users.setImageUri(receiverImage);
        activityChatBinding.setUsers(users);
        activityChatBinding.chatName.setText(receiverName);
        messageAdapter= activityChatBinding.massageViewAdapter;
        chatPersenter.sendDataToModel(receiverUid,receiverImage);
        messagesArrayList = new ArrayList<>();
        adapter = new ChatMessageAdapter(ChatActivity.this, messagesArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        messageAdapter.setLayoutManager(linearLayoutManager);
        messageAdapter.setAdapter(adapter);


    }

    @Override
    public void dataToRecyclerView(ArrayList<Messages> messages) {
        messagesArrayList.clear();
        messagesArrayList.addAll(messages);
        adapter.notifyDataSetChanged();
        messageAdapter.scrollToPosition(adapter.getItemCount() -1);

    }
    public void sendBtn() {
        String message = activityChatBinding.edittextMsg.getText().toString();
        if (message.isEmpty()) {
            Toast.makeText(ChatActivity.this, "Please enter Valid Message", Toast.LENGTH_SHORT).show();
            return;
        }
        activityChatBinding.edittextMsg.setText("");
        Date date = new Date();

        Messages messages = new Messages(message, senderUid, date.getTime());
        chatPersenter.sendMessage(messages);
    }


}

