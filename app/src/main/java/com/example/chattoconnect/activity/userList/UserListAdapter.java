package com.example.chattoconnect.activity.userList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chattoconnect.activity.chatActivity.ChatActivity;
import com.example.chattoconnect.databinding.UserRecyclerviewRowBinding;
import com.example.chattoconnect.modal.Users;

import java.util.ArrayList;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    Context homeActivity;
    ArrayList<Users> usersArrayList;

    public UserListAdapter(UserListActivity homeActivity, ArrayList<Users> usersArrayList) {
        this.homeActivity = homeActivity;
        this.usersArrayList = usersArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        UserRecyclerviewRowBinding userRecyclerviewRowBinding = UserRecyclerviewRowBinding.inflate(layoutInflater,parent,false);
        return  new ViewHolder(userRecyclerviewRowBinding);
      /*  View view = LayoutInflater.from(homeActivity).inflate(R.layout.user_recyclerview_row,parent,false);
        return new ViewHolder(view);*/
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Users users = usersArrayList.get(position);
        holder.userRecyclerviewRowBinding.setUsers(users);
        holder.userRecyclerviewRowBinding.executePendingBindings();
      //  holder.bind(users);
     /*   holder.user_name.setText(users.getName());
        Picasso.get().load(users.getImageUri()).placeholder(R.drawable.profile).into(holder.user_profile);*/
        holder.userRecyclerviewRowBinding.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homeActivity, ChatActivity.class);
                intent.putExtra("name", users.getName());
                intent.putExtra("ReciverImage", users.getImageUri());
                intent.putExtra("uid", users.getUid());
                homeActivity.startActivity(intent);
            }
        });
        holder.userRecyclerviewRowBinding.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homeActivity, ChatActivity.class);
                intent.putExtra("name", users.getName());
                intent.putExtra("ReciverImage", users.getImageUri());
                intent.putExtra("uid", users.getUid());
                homeActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersArrayList.size();
    }

   static class ViewHolder extends RecyclerView.ViewHolder{
      //  CircleImageView user_profile;
     //   TextView user_name;
        UserRecyclerviewRowBinding userRecyclerviewRowBinding;

        public ViewHolder(@Nullable UserRecyclerviewRowBinding userRecyclerviewRowBinding)
        {
            super(userRecyclerviewRowBinding.getRoot());
            this.userRecyclerviewRowBinding = userRecyclerviewRowBinding;

           /* user_name = itemView.findViewById(R.id.user_name);

            user_profile = itemView.findViewById(R.id.user_image);*/
        }
     /*   public void bind(Users users)
        {
           /* user_name.setText(users.getName());
            Picasso.get().load(users.getImageUri()).placeholder(R.drawable.profile).into(user_profile);

        }*/
    }
}

