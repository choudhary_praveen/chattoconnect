package com.example.chattoconnect.activity.chatActivity;

import androidx.databinding.ObservableField;

import com.example.chattoconnect.activity.userList.UserListPresenter;
import com.example.chattoconnect.modal.Messages;
import com.example.chattoconnect.modal.Users;

import java.util.ArrayList;
import java.util.List;

public class ChatPersenter implements ChatContractInterface.Presenter {
    private ChatContractInterface.View view;
    private ChatContractInterface.Model model;

    public ChatPersenter(ChatContractInterface.View view) {
        this.view = view;
        this.model = new ChatListModelService(this);
    }

    @Override
    public void sendMessageToView(ArrayList<Messages> messages) {

        view.dataToRecyclerView(messages);

    }

    @Override
    public void sendDataToModel(String receverUid, String RImage) {
        model.dataFromPersenter(receverUid,RImage);

    }



    @Override
    public void sendMessage(Messages messages) {
        model.sendMessageModel(messages);

    }
}
