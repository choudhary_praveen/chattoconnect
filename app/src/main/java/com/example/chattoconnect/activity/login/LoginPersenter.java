package com.example.chattoconnect.activity.login;


import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.example.chattoconnect.databinding.ActivityLoginBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class LoginPersenter implements LoginContractInterface.Presenter  {

    private LoginContractInterface.View view;
    private LoginContractInterface.Model model;


    public LoginPersenter(LoginContractInterface.View view) {
        this.view = view;
        this.model = new LoginModelService(this);
    }




    @Override
    public void doLogin(String email,String password)
    {
          model.doSignIn(email, password);
    }

    @Override
    public void loginfailed(String error) {
        view.showLoginFailed(error);
    }

    @Override
    public void progressDialog(boolean dialog) {
         if(dialog==true)
         {
             view.showProgress();
         }
         else {
             view.hideProgress();
         }
    }

    @Override
    public void signIn() {
        view.signInBtn1();
    }


}
