package com.example.chattoconnect.activity.registration;

import android.net.Uri;

import com.example.chattoconnect.modal.Users;

public interface RegistrationContractInterface  {
    interface View {

        void showLoginFailed(String error);
        void signUnBtn1();
        void showProgress();
        void hideProgress();

    }
    interface Presenter {
        void doSignUp(String name,String email,String password,String cnf_password,Uri imageUri);
        void loginfailed(String error);
        void progressDialog(boolean dialog);
        void signUp();
        void getImageUri(String URI);
    }
    interface Model{
        void doSignIn(String name,String email,String password,String cnf_password,Uri imageUri);
        void setImageUri(String uri);
    }
}
