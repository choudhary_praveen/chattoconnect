package com.example.chattoconnect.activity.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.chattoconnect.R;
import com.example.chattoconnect.activity.registration.RegistrationActivity;
import com.example.chattoconnect.activity.userList.UserListActivity;
import com.example.chattoconnect.databinding.ActivityLoginBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements LoginContractInterface.View {
    ActivityLoginBinding activityLoginBinding;

    private LoginPersenter loginPersenter;
    ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.setIlogin(this);
        loginPersenter = new LoginPersenter(this);


        Log.d("debug1", "inLoginActivity");
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);




    }
    public void  signInBtn(){
        progressDialog.show();
        String email = activityLoginBinding.loginEmail.getText().toString();
        String password = activityLoginBinding.loginPassword.getText().toString();

        loginPersenter.doLogin(email,password);


    }

    public void signUpBtn() {
        Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(intent);

    }
    @Override
    public void showLoginFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {

        progressDialog.dismiss();
    }
    @Override
    public void signInBtn1() {

        Intent intent = new Intent(LoginActivity.this, UserListActivity.class);
        startActivity(intent);
        finish();

    }





}