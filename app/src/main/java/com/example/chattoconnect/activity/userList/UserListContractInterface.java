package com.example.chattoconnect.activity.userList;

import androidx.annotation.NonNull;

import com.example.chattoconnect.modal.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;

public interface UserListContractInterface {
    interface View {
       void logoutActivity();
       void updateList(ArrayList<Users> list);
    }
    interface Presenter {
        void loggedOut();
        void logOutModel();
        void onModelUpdate(ArrayList<Users> list);

    }
    interface Model{

         void getDataFromServer();
         void doLogout();
         void invoke();
         void dataFromModel(ArrayList<Users> list);
    }
}
