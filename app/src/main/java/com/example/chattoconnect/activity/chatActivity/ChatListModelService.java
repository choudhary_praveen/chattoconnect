package com.example.chattoconnect.activity.chatActivity;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.chattoconnect.R;
import com.example.chattoconnect.modal.Messages;
import com.example.chattoconnect.modal.Users;
import com.example.chattoconnect.pushNotification.APIService;
import com.example.chattoconnect.pushNotification.Client;
import com.example.chattoconnect.pushNotification.Data;
import com.example.chattoconnect.pushNotification.MyResponse;
import com.example.chattoconnect.pushNotification.Sender;
import com.example.chattoconnect.pushNotification.Token;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatListModelService implements ChatContractInterface.Model{
    String receiverImage,receiverName,receiverUid,senderUid,senderRoom,receiverRoom;
    String userid;
    public static String sImage;
    public static String rImage;
    FirebaseDatabase database ;
    FirebaseAuth firebaseAuth;
    FirebaseUser fuser;
    private ChatPersenter chatPersenter;
    APIService apiService;
    boolean notify ;
    DatabaseReference reference;
    DatabaseReference chatReference;
    ArrayList<Messages> messagesArrayList = new ArrayList<>();

    public ChatListModelService (ChatContractInterface.Presenter chatPersenter)
    {
        this.chatPersenter = (ChatPersenter) chatPersenter;
//        reference = database.getReference().child("user").child(firebaseAuth.getUid());
  //      chatReference = database.getReference().child("chats").child(senderRoom).child("messages");
    }

    public ChatListModelService()
    {

    }




    public void getDataOnRefresh() {
        fuser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        senderUid = firebaseAuth.getUid();
        senderRoom = senderUid + receiverUid;
        receiverRoom = receiverUid + senderUid;

        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);
         reference = database.getReference().child("user").child(firebaseAuth.getUid());
         chatReference = database.getReference().child("chats").child(senderRoom).child("messages");

        chatReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                messagesArrayList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Messages messages = dataSnapshot.getValue(Messages.class);
                    messagesArrayList.add(messages);

                }
                chatPersenter.sendMessageToView(messagesArrayList);
            //    adapter.notifyDataSetChanged();
            //    messageAdapter.scrollToPosition(adapter.getItemCount() - 1);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                sImage = snapshot.child("imageUri").getValue().toString();
                rImage = receiverImage;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        updateToken(FirebaseInstanceId.getInstance().getToken());

    }










    @Override
    public void sendMessageModel(Messages messages) {
        messages.setSenderId(senderUid);

        notify = true;
        database.getReference().child("chats")
                .child(senderRoom)
                .child("messages")
                .push()
                .setValue(messages).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                database.getReference().child("chats")
                        .child(receiverRoom)
                        .child("messages")
                        .push().setValue(messages).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
            }
        });
        final String msg = messages.getMessage();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference = FirebaseDatabase.getInstance().getReference("user").child(senderUid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Users user = dataSnapshot.getValue(Users.class);
                if(user == null){
                    Log.d("debuger","User is null");
                    return;
                }
                if (notify) {
                    sendNotifiaction(receiverUid, user.getName(), msg);
                }
                notify = false;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void dataFromPersenter(String receverUid, String RImage) {
        this.receiverUid = receverUid;
        rImage = RImage;
        getDataOnRefresh();
    }

    private void updateToken(String token)
    {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token token1 = new Token(token);
        reference.child(fuser.getUid()).setValue(token1);
    }
    private void sendNotifiaction(String receiver, final String username, final String message){
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Token token = snapshot.getValue(Token.class);
                    Data data = new Data(fuser.getUid(), R.mipmap.ic_launcher, username+": "+message, "New Message",
                            receiverUid);

                    Sender sender = new Sender(data, token.getToken());

                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if (response.code() == 200){
                                        if (response.body().success != 1){
                                         //   Toast.makeText(ChatActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {
                                   // Toast.makeText(ChatActivity.this, "Failed it", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
