package com.example.chattoconnect.activity.registration;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.chattoconnect.activity.login.LoginActivity;
import com.example.chattoconnect.R;
import com.example.chattoconnect.activity.userList.UserListActivity;
import com.example.chattoconnect.databinding.ActivityRegistrationBinding;
import com.example.chattoconnect.modal.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


public class RegistrationActivity extends AppCompatActivity implements RegistrationContractInterface.View{
    ActivityRegistrationBinding activityRegistrationBinding;
    Uri imageUri;
    ProgressDialog progressDialog;
    private RegistrationPresenter registrationPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityRegistrationBinding = DataBindingUtil.setContentView(this,R.layout.activity_registration);
        activityRegistrationBinding.setIreq(this);
        registrationPresenter = new RegistrationPresenter(this);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        Log.d("debug1","inRegActivity");



    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode==10)
        {
            if(data!=null)
            {
                imageUri = data.getData();
                activityRegistrationBinding.profileImage.setImageURI(imageUri);
                registrationPresenter.getImageUri(imageUri.toString());
            }
        }
    }
    public void profileClick()
    {
        Log.d("debuuger","clicked profile");
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 10);

    }
    public void signUpBtn()
    {
        progressDialog.show();
        String name,email,password,cnf_password;
        name = activityRegistrationBinding.name.getText().toString();
        email = activityRegistrationBinding.email.getText().toString();
        password = activityRegistrationBinding.password.getText().toString();
        cnf_password = activityRegistrationBinding.cnfPassword.getText().toString();

        registrationPresenter.doSignUp(name,email,password,cnf_password,imageUri);

    }
    public void signInBtn()
    {
        Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoginFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void signUnBtn1() {
        progressDialog.show();
        Intent intent = new Intent(RegistrationActivity.this, UserListActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();

    }





}